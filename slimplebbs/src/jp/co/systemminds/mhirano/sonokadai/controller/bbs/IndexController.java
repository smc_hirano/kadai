package jp.co.systemminds.mhirano.sonokadai.controller.bbs;

import java.util.List;

import jp.co.systemminds.mhirano.sonokadai.model.bbs.Head;
import jp.co.systemminds.mhirano.sonokadai.service.bbs.BbsService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class IndexController extends Controller {

    @Override
    public Navigation run() throws Exception {
//        return forward("index.jsp");
        BbsService service = new BbsService();
        List<Head> headList = service.getAll();
        requestScope("headList",headList);
        return forward("index.jsp");
    }
}
