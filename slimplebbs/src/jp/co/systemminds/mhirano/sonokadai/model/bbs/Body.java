package jp.co.systemminds.mhirano.sonokadai.model.bbs;

import java.io.Serializable;

import jp.co.systemminds.mhirano.sonokadai.meta.bbs.HeadMeta;

import com.google.appengine.api.datastore.Key;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.InverseModelRef;
import org.slim3.datastore.Model;

@Model(schemaVersion = 1)
public class Body implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;

    /**
     * Returns the key.
     *
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    /**
     * Returns the version.
     *
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    // 記事本文
    @Attribute(lob=true)
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    // Headへの1対1の関連
    @Attribute(persistent=false)
    private InverseModelRef<Head, Body> headRef =
        new InverseModelRef<Head, Body>(Head.class, HeadMeta.get().bodyRef, this);

    public InverseModelRef<Head, Body> getHeadRef() {
        return headRef;
    }

//    public void setHeadRef(InverseModelRef<Head, Body> headRef) {
//        this.headRef = headRef;
//    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Body other = (Body) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }
}
