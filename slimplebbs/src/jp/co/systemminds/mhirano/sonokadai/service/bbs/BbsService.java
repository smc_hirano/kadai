package jp.co.systemminds.mhirano.sonokadai.service.bbs;

import java.util.List;

import org.slim3.datastore.Datastore;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Transaction;

import jp.co.systemminds.mhirano.sonokadai.meta.bbs.BodyMeta;
import jp.co.systemminds.mhirano.sonokadai.meta.bbs.CommentMeta;
import jp.co.systemminds.mhirano.sonokadai.meta.bbs.HeadMeta;
import jp.co.systemminds.mhirano.sonokadai.model.bbs.Body;
import jp.co.systemminds.mhirano.sonokadai.model.bbs.Comment;
import jp.co.systemminds.mhirano.sonokadai.model.bbs.Head;

public class BbsService {

    public List<Head> getAll() {
        // // TODO 自動生成されたメソッド・スタブ
        // return null;
        HeadMeta m = HeadMeta.get();
        return Datastore.query(m).sort(m.postDate.desc).asList();
    }

    public void insert(Head head, Body body) throws Exception {
        // TODO 自動生成されたメソッド・スタブ

        head.setKey(Datastore.allocateId(HeadMeta.get()));
        body.setKey(Datastore.allocateId(head.getKey(), BodyMeta.get()));
        head.getBodyRef().setModel(body);

        Transaction tx = Datastore.beginTransaction();

        try {
            Datastore.put(tx, head, body);
            Datastore.commit(tx);
        } catch (Exception e) {
            // // TODO 自動生成された catch ブロック
            // e.printStackTrace();
            if (tx.isActive()) {
                Datastore.rollback(tx);
            }
        }

    }

    public Head get(Key headKey) {
        // // TODO 自動生成されたメソッド・スタブ
        // return null;
        return Datastore.getOrNull(HeadMeta.get(), headKey);
    }

    public void delete(Key key) throws Exception {
        // TODO 自動生成されたメソッド・スタブ
        Transaction tx = Datastore.beginTransaction();

        try {
            Head head = Datastore.get(tx, HeadMeta.get(), key);
            Key bodyKey = head.getBodyRef().getKey();
            Datastore.delete(tx, key, bodyKey);
            Datastore.commit(tx);
        } catch (Exception e) {
            // // TODO 自動生成された catch ブロック
            // e.printStackTrace();
            if (tx.isActive()) {
                Datastore.rollback(tx);
            }
            throw e;
        }

    }

    public void update(Head storedHead, Body storedBody) throws Exception {
        // TODO 自動生成されたメソッド・スタブ
        Transaction tx = Datastore.beginTransaction();

        try {
            Datastore.get(tx,HeadMeta.get(),storedHead.getKey(),storedHead.getVersion());
            Datastore.put(tx, storedHead,storedBody);
            Datastore.commit(tx);
        } catch (Exception e) {
            // // TODO 自動生成された catch ブロック
            // e.printStackTrace();
            if (tx.isActive()) {
                Datastore.rollback(tx);
            }
            throw e;
        }

    }

    public void insert(Head storedHead, Comment comment) throws Exception {
        // TODO 自動生成されたメソッド・スタブ
        long newCommentId = storedHead.getLastCommentId()+1L;

        Key commKey = Datastore.createKey(storedHead.getKey(), CommentMeta.get(),newCommentId);
        comment.setKey(commKey);
        comment.getHeadRef().setModel(storedHead);

        storedHead.setLastCommentId(newCommentId);
        storedHead.setLastCommentDate(comment.getPostDate());

        Transaction tx = Datastore.beginTransaction();

        try {
            Datastore.get(tx,HeadMeta.get(),storedHead.getKey(),storedHead.getVersion());
            Datastore.put(tx, storedHead,comment);
            Datastore.commit(tx);
        } catch (Exception e) {
            // // TODO 自動生成された catch ブロック
            // e.printStackTrace();
            if (tx.isActive()) {
                Datastore.rollback(tx);
            }
            throw e;
        }

    }

}
