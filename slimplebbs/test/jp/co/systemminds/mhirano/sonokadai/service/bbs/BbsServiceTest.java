package jp.co.systemminds.mhirano.sonokadai.service.bbs;

import java.util.Date;
import java.util.List;

import jp.co.systemminds.mhirano.sonokadai.model.bbs.Body;
import jp.co.systemminds.mhirano.sonokadai.model.bbs.Comment;
import jp.co.systemminds.mhirano.sonokadai.model.bbs.Head;

import org.slim3.tester.AppEngineTestCase;
import org.slim3.util.DateUtil;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class BbsServiceTest extends AppEngineTestCase {

    private BbsService service = new BbsService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));

        // 記事一覧の取得
        List<Head> HeadList = service.getAll();

        // ========== assertion start ========== //
        // 初回は0件であること
        assertNotNull(HeadList);
        assertTrue(HeadList.isEmpty());
        // ========== assertion end ========== //

        // 記事の作成
        Head head = new Head();
        head.setSubject("初めての記事");
        head.setUsername("ユーザ１");
        head.setPostDate(new Date());
        // 本文の作成
        Body body = new Body();
        body.setText("初めての本文です。");
        // データストアへ更新
        service.insert(head, body);

        // 更新後の記事一覧の取得
        HeadList = service.getAll();
        // ========== assertion start ========== //
        // 投稿後の記事一覧は1件であること
        assertNotNull(HeadList);
        assertTrue(HeadList.size() == 1);
        // 以降、1件の中身のチェック
        Head storedHead = HeadList.get(0);

//        assertNotNull(storedHead);

        // 新規作成したHeadと、登録後再取得したHeadが等しいこと
        assertEqualsHead(head, storedHead);
        // 登録後のバージョンが1であること
        assertEquals(storedHead.getVersion(), Long.valueOf(1L));
        // ========== assertion end ========== //

     // =====================================
        // 記事の一件取得
        // =====================================
        // 記事一覧から一つの記事が選択された想定で再取得
        storedHead = service.get(storedHead.getKey());
        // ========== assertion start ========== //
        // 指定されたKeyの記事が取得できていること
        assertNotNull(storedHead);
        // 先に登録したHeadであること
        assertEqualsHead(head, storedHead);
        // バージョンが1であること
        assertEquals(storedHead.getVersion(), Long.valueOf(1L));
        // ========== assertion end ========== //

     // =====================================
        // 記事の上書き更新
        // =====================================
        // 記事の変更
        storedHead.setSubject("修正した記事");
        Body storedBody = storedHead.getBodyRef().getModel();
        storedBody.setText("上書き更新した本文です。");
        // データストアへ上書き更新
        service.update(storedHead, storedBody);
        // 更新後の記事一覧の取得
        HeadList = service.getAll();
        // ========== assertion start ========== //
        // 投稿後の記事一覧は1件であること
        assertNotNull(HeadList);
        assertTrue(HeadList.size() == 1);
        // 以降、1件の中身のチェック
        Head updatedHead = HeadList.get(0);
        // 修正したHeadと、更新後再取得したHeadが等しいこと
        assertEqualsHead(storedHead, updatedHead);
        // 上書き更新したのでバージョンが2になっていること
        assertEquals(updatedHead.getVersion(), Long.valueOf(2L));
        // ========== assertion end ========== //

        // =====================================
        // 記事の削除
        // =====================================
        // 記事の削除
        service.delete(updatedHead.getKey());
        // 削除後の記事の取得
        storedHead = service.get(updatedHead.getKey());
        // 削除後の記事一覧の取得
        HeadList   = service.getAll();
        // ========== assertion start ========== //
        // この記事は削除されているのでNullであること
        assertNull(storedHead);
        // 記事一覧は０件のリストであること
        assertNotNull(HeadList);
        assertTrue(HeadList.isEmpty());
        // ========== assertion end ========== //



    }

    private void assertEqualsHead(Head head1, Head head2) {
        // TODO 自動生成されたメソッド・スタブ
        // 共にNullではないこと
        assertNotNull(head1);
        assertNotNull(head2);
        // 記事タイトル、ユーザ名、投稿日時が等しいこと
        assertEquals(head1.getSubject(), head2.getSubject());
        assertEquals(head1.getUsername(), head2.getUsername());
        assertEquals(head1.getPostDate(), head2.getPostDate());
        // 共にリレーションシップからBodyが取得できること
        Body body1 = head1.getBodyRef().getModel();
        Body body2 = head2.getBodyRef().getModel();
        assertNotNull(body1);
        assertNotNull(body2);
        // 本文が等しいこと
        assertEquals(body1.getText(), body2.getText());
    }


    @Test
    public void postCommentTest() throws Exception{
        // 記事の作成
        Head head = new Head();
        head.setSubject("初めての記事");
        head.setUsername("ユーザ１");
        head.setPostDate(new Date());
        // 本文の作成
        Body body = new Body();
        body.setText("初めての本文です。");
        // データストアへ更新
        service.insert(head, body);
        // 記事一覧の取得
        List<Head> headList = service.getAll();
        // 投稿後の記事一覧は1件であること
        assertNotNull(headList);
        assertTrue(headList.size() == 1);
        // 記事を取得
        Head storedHead = headList.get(0);
        // この記事のコメント一覧を取得
        List<Comment> commentList = storedHead.getCommentRef().getModelList();
        // ========== assertion start ========== //
        // コメント投稿前なので０件であること
        assertNotNull(commentList);
        assertTrue(commentList.isEmpty());
        // ========== assertion end ========== //

        // コメントの作成
        Comment comment =
            createComment("ユーザ２", new Date(), "コメントの投稿です。");
        // データストアへ更新
        service.insert(storedHead, comment);
        // 記事の再取得
        storedHead = service.get(storedHead.getKey());
        // コメント一覧の再取得
        commentList = storedHead.getCommentRef().getModelList();
        // ========== assertion start ========== //
        // １件のコメントが投稿されていること
        assertNotNull(commentList);
        assertTrue(commentList.size() == 1);
        // 以降、1件の中身のチェック
        Comment postedComment = commentList.get(0);
        assertEquals(comment.getUsername(), postedComment.getUsername());
        assertEquals(comment.getComment(), postedComment.getComment());
        assertEquals(comment.getPostDate(), postedComment.getPostDate());
        // 主キーであるコメントIDが１であること
        assertEquals(postedComment.getKey().getId(), 1L);

        // コメント件数が１であること
        assertTrue(storedHead.getLastCommentId() == 1L);
        // 最終コメント日時がコメント投稿日時と同じであること
        assertEquals(storedHead.getLastCommentDate(), comment.getPostDate());
        // ========== assertion end ========== //
    }

    private Comment createComment(String username, Date postDate, String text) throws Exception {
        // コメントの作成
        Comment comment = new Comment();
        comment.setUsername(username);
        comment.setComment(text);
        comment.setPostDate(postDate);
        return comment;
    }

 // 記事一覧のソート順テスト（投稿日時降順）
    @Test
    public void headListSortOrderTest() throws Exception {
        // 投稿日付をランダムに並べ記事を順次登録
        insertHead("題名", "ユーザ", toDate("2010/10/18 15:45:00"), "本文");
        insertHead("題名", "ユーザ", toDate("2011/01/01 00:00:00"), "本文");
        insertHead("題名", "ユーザ", toDate("2011/01/01 12:34:56"), "本文");
        insertHead("題名", "ユーザ", toDate("2011/05/05 10:30:30"), "本文");
        insertHead("題名", "ユーザ", toDate("2010/10/22 15:45:45"), "本文");
        insertHead("題名", "ユーザ", toDate("2012/01/01 23:59:10"), "本文");
        insertHead("題名", "ユーザ", toDate("2011/05/05 11:30:30"), "本文");
        insertHead("題名", "ユーザ", toDate("2010/10/20 15:45:00"), "本文");
        insertHead("題名", "ユーザ", toDate("2010/10/22 15:45:00"), "本文");
        // 一覧の取得
        List<Head> headList = service.getAll();
        assertNotNull(headList);
        assertTrue(headList.size() == 9);
        // 投稿日付の降順に並んでいるかチェック
        Head preHead = null;
        for (Head head : headList) {
            if (preHead == null) {
                preHead = head;
                continue;
            }
            // 一件前の日付より古い投稿日付であること
            assertTrue(head.getPostDate().before(preHead.getPostDate()));
            preHead = head;
        }
    }

    // コメント一覧のソート順テスト（コメントID順）
    @Test
    public void commentListSortOrderTest() throws Exception {
        // 記事の登録
        insertHead("題名", "ユーザ", new Date(), "本文");
        // 一覧の取得
        List<Head> headList = service.getAll();
        assertNotNull(headList);
        assertTrue(headList.size() == 1);
        // 記事の取得
        Head head = headList.get(0);
        // この記事にコメントを1000件投稿してみる
        int max = 1000;
        for (int i = 0; i < max; i++) {
            Comment newComment = createComment("ユーザ", new Date(), "コメント");
            service.insert(head, newComment);
        }
        // コメント投稿された記事を再取得
        head = service.get(head.getKey());
        // コメント一覧をリレーションシップで取得
        List<Comment> commentList = head.getCommentRef().getModelList();
        assertNotNull(commentList);
        assertTrue(commentList.size() == max);
        // コメントID昇順に並んでいるかチェック
        Comment preComment = null;
        for (Comment comment : commentList) {
            if (preComment == null) {
                preComment = comment;
                continue;
            }
            // 一件前のコメントIDより＋１大きいIDであること
            assertEquals(comment.getKey().getId(), preComment.getKey().getId() + 1L);
            preComment = comment;
        }
        // 記事のコメント数が1000であること
        assertEquals(head.getLastCommentId(), Long.valueOf(max));
        // 記事の最終コメント日付が最後のコメントの日付と同じであること
        assertEquals(head.getLastCommentDate(), preComment.getPostDate());
    }

    private Date toDate(String yyyyMMddHHmmss) {
        return DateUtil.toDate(yyyyMMddHHmmss, "yyyy/MM/dd HH:mm:ss");
    }

    // 記事を新規登録する
    private void insertHead(String subject, String username, Date postDate, String text) throws Exception {
        // 記事の作成
        Head head = new Head();
        head.setSubject(subject);
        head.setUsername(username);
        head.setPostDate(postDate);
        // 本文の作成
        Body body = new Body();
        body.setText(text);
        // データストアへ更新
        service.insert(head, body);
    }

}
